Name:               xcb-util
Version:            0.4.1
Release:            1
Summary:            XCB utility modules

License:            MIT
URL:                https://xcb.freedesktop.org/
Source0:            https://xcb.freedesktop.org/dist/%{name}-%{version}.tar.gz

BuildRequires:      gcc libxcb-devel
Requires:           libxcb glibc

%description
The X protocol C-language Binding (XCB) is a replacement for Xlib
featuring a small footprint, latency hiding, direct access to the
protocol, improved threading support, and extensibility.

%package devel
Summary:            Header files for xcb-util
Requires:           %{name} = %{version}-%{release}

%description devel
Header package for xcb-util

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --with-pic --disable-silent-rules
%make_build

%install
%make_install
%delete_la_and_a
%ldconfig_scriptlets

%check
make check

%files
%defattr(-,root,root)
%license COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/*.pc
%{_includedir}/xcb/*.h
%{_libdir}/*.so

%files help
%doc NEWS README.md


%changelog
* Wed Jul 19 2023 zhangpan <zhangpan103@h-partners.com> - 0.4.1-1
- update to 0.4.1

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 0.4.0-14
- Rebuild for next release

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.0-13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add requires

* Wed Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 0.4.0-12
- Package init
